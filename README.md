# AngularTodoList

A todoList app using Angular inspired from this [dribble design](https://dribbble.com/shots/15261921-Dashboard-for-workflow)

### Made using
- Angular
- Angular Material 
- Tailwind CSS

---

## How to start 

#### Prerequisites
- Node
- Angular CLI

###### 1. Install node_nodules
``` terminal 
npm i 
```

###### 2. Run (Serve) angular app
``` terminal
ng serve
```