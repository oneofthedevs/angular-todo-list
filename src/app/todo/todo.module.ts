import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoRoutingModule } from './todo-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CreateComponent } from './components/create/create.component';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [DashboardComponent, CreateComponent],
  imports: [CommonModule, TodoRoutingModule, CoreModule],
})
export class TodoModule {}
