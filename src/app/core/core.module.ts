import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialImports } from './material/material';

@NgModule({
  declarations: [],
  imports: [CommonModule, MaterialImports],
})
export class CoreModule {}
