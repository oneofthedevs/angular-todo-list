import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'main-side-drawer',
  templateUrl: './side-drawer.component.html',
  styleUrls: ['./side-drawer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SideDrawerComponent {}
